package com.vabland.api.user.domain.service;

import com.vabland.api.user.infrastructure.nosql.UserEntity;
import com.vabland.api.user.infrastructure.nosql.UserEntityRepository;
import com.vabland.api.user.presentation.request.UserCreationRequest;
import com.vabland.api.user.presentation.response.UserResponse;
import com.vabland.api.user.utils.fixture.Fixture;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

import static com.vabland.api.user.utils.random.RandomAttributeUtils.randomEncodedPassword;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@DisplayName("UserCreationService Class")
class UserCreationServiceTest {

    @Mock
    private PasswordEncoder passwordEncoder;

    @Mock
    private UserEntityRepository userEntityRepository;

    @InjectMocks
    private UserCreationService userCreationService;

    @Nested
    @DisplayName("create Method")
    class CreateMethod {

        private UserCreationRequest userCreationRequest;
        private String encodedPassword;
        private UserEntity userEntity;

        @BeforeEach
        void setUp() {
            encodedPassword = randomEncodedPassword();
            userCreationRequest = Fixture.userCreationRequest().build();
            userEntity = Fixture.userEntity()
                    .givenName(userCreationRequest.getGivenName())
                    .familyName(userCreationRequest.getFamilyName())
                    .email(userCreationRequest.getEmail())
                    .encodedPassword(encodedPassword)
                    .build();

            when(passwordEncoder.encode(userCreationRequest.getPassword())).thenReturn(encodedPassword);
            when(userEntityRepository.save(any())).thenReturn(userEntity);
        }

        @Test
        @DisplayName("should save UserEntity with values from request and with the password encoded")
        void shouldSaveUserEntityWithValuesFromRequestAndEncodedPassword() {
            ArgumentCaptor<UserEntity> captor = ArgumentCaptor.forClass(UserEntity.class);

            userCreationService.create(userCreationRequest);

            verify(userEntityRepository).save(captor.capture());
            UserEntity savedUserEntity = captor.getValue();
            assertThat(savedUserEntity.getGivenName()).isEqualTo(userCreationRequest.getGivenName());
            assertThat(savedUserEntity.getFamilyName()).isEqualTo(userCreationRequest.getFamilyName());
            assertThat(savedUserEntity.getEmail()).isEqualTo(userCreationRequest.getEmail());
            assertThat(savedUserEntity.getEncodedPassword()).isEqualTo(encodedPassword);
        }

        @Test
        @DisplayName("should returns UserResponse using data from saved UserEntity")
        void shouldReturnUserResponseUsingDataFromSavedUserEntity() {
            UserResponse userResponse = userCreationService.create(userCreationRequest);

            assertThat(userResponse.getId()).isEqualTo(userEntity.getId());
            assertThat(userResponse.getGivenName()).isEqualTo(userEntity.getGivenName());
            assertThat(userResponse.getFamilyName()).isEqualTo(userEntity.getFamilyName());
            assertThat(userResponse.getEmail()).isEqualTo(userEntity.getEmail());
        }

    }

}