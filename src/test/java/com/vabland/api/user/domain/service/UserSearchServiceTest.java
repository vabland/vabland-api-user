package com.vabland.api.user.domain.service;

import com.vabland.api.user.infrastructure.nosql.UserEntity;
import com.vabland.api.user.infrastructure.nosql.UserEntityRepository;
import com.vabland.api.user.presentation.response.UserResponse;
import com.vabland.api.user.utils.fixture.Fixture;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.Optional;

import static com.vabland.api.user.utils.random.RandomAttributeUtils.randomEmail;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@DisplayName("UserSearchService Class")
class UserSearchServiceTest {

    @Mock
    private UserEntityRepository userEntityRepository;

    @InjectMocks
    private UserSearchService userSearchService;

    @Nested
    @DisplayName("findByEmail Method")
    class FindByEmailMethod {

        @Test
        @DisplayName("Should returns an Optional filled with a UserResponse when UserEntityRepository finds an user by email")
        void returnOptionalWithUserWhenRepositoryFindsIt() {
            String email = randomEmail();
            UserEntity userEntity = Fixture.userEntity()
                    .email(email)
                    .build();
            when(userEntityRepository.findByEmail(email)).thenReturn(Arrays.asList(userEntity));

            Optional<UserResponse> response = userSearchService.findByEmail(email);

            assertThat(response.isPresent()).isTrue();
            assertThat(response.get().getId()).isEqualTo(userEntity.getId());
            assertThat(response.get().getGivenName()).isEqualTo(userEntity.getGivenName());
            assertThat(response.get().getFamilyName()).isEqualTo(userEntity.getFamilyName());
            assertThat(response.get().getEmail()).isEqualTo(userEntity.getEmail());
            assertThat(response.get().getEncodedPassword()).isEqualTo(userEntity.getEncodedPassword());
        }

        @Test
        @DisplayName("Should returns an empty Optional when UserEntityRepository doesn't find any user by email")
        void returnOptionalEmptyWhenRepositoryDoesNotFindIt() {
            String email = randomEmail();
            when(userEntityRepository.findByEmail(email)).thenReturn(Arrays.asList());

            Optional<UserResponse> response = userSearchService.findByEmail(email);

            assertThat(response.isPresent()).isFalse();
        }

    }
}