package com.vabland.api.user.utils.fixture;

public class Fixture {

    public static UserBuilder userEntity() {
        return new UserBuilder();
    }

    public static UserCreationRequestBuilder userCreationRequest() {
        return new UserCreationRequestBuilder();
    }

    public static UserResponseBuilder userResponse() {
        return new UserResponseBuilder();
    }

}
