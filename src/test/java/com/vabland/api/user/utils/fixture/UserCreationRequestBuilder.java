package com.vabland.api.user.utils.fixture;

import com.vabland.api.user.presentation.request.UserCreationRequest;

import static com.vabland.api.user.utils.random.RandomAttributeUtils.*;

public class UserCreationRequestBuilder {

    private String givenName = randomName();
    private String familyName = randomName();
    private String email  = randomEmail();
    private String password = randomPassword();
    private String matchingPassword = password;

    UserCreationRequestBuilder() {}

    public UserCreationRequestBuilder password(String password) {
        this.password = password;
        return this;
    }

    public UserCreationRequestBuilder matchingPassword(String matchingPassword) {
        this.matchingPassword = matchingPassword;
        return this;
    }

    public UserCreationRequest build() {
        return new UserCreationRequest(givenName, familyName, email, password, matchingPassword);
    }
}
