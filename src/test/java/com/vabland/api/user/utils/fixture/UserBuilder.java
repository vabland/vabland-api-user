package com.vabland.api.user.utils.fixture;

import com.vabland.api.user.infrastructure.nosql.UserEntity;

import java.time.LocalDateTime;

import static com.vabland.api.user.utils.random.RandomAttributeUtils.*;

public class UserBuilder {

    private String id = randomId();
    private String givenName = randomName();
    private String familyName = randomName();
    private String email = randomEmail();
    private String encodedPassword = randomPassword();
    private LocalDateTime createdAt = LocalDateTime.now();
    private LocalDateTime updatedAt = LocalDateTime.now();

    UserBuilder() { }

    public UserBuilder givenName(String givenName) {
        this.givenName = givenName;
        return this;
    }

    public UserBuilder familyName(String familyName) {
        this.familyName = familyName;
        return this;
    }

    public UserBuilder email(String email) {
        this.email = email;
        return this;
    }

    public UserBuilder encodedPassword(String encodedPassword) {
        this.encodedPassword = encodedPassword;
        return this;
    }

    public UserEntity build() {
        return new UserEntity(id, givenName, familyName, email, encodedPassword, createdAt, updatedAt);
    }

}
