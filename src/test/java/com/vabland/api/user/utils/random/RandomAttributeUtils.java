package com.vabland.api.user.utils.random;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.Random;
import java.util.UUID;

import static org.apache.commons.lang3.RandomStringUtils.*;

public class RandomAttributeUtils {

    public static String randomId() {
        return UUID.randomUUID().toString();
    }

    public static String randomName() {
        if (new Random().nextBoolean()) {
            return randomAlphabetic(5) + " " + randomAlphabetic(6);
        }
        return randomAlphabetic(8);
    }

    public static String randomEmail() {
        return randomAlphanumeric(8) + "@" + randomAlphanumeric(5) + ".com";
    }

    public static String randomPassword() {
        return randomNumeric(1) +
                randomAlphabetic(1).toUpperCase() +
                randomAlphabetic(1).toLowerCase() +
                randomAlphanumeric(8);
    }

    public static String randomEncodedPassword() {
        return new BCryptPasswordEncoder().encode(randomPassword());
    }

}
