package com.vabland.api.user.utils.fixture;

import com.vabland.api.user.presentation.response.UserResponse;

import static com.vabland.api.user.utils.random.RandomAttributeUtils.*;

public class UserResponseBuilder {

    private String id = randomId();
    private String givenName = randomName();
    private String familyName = randomName();
    private String email  = randomEmail();
    private String encodedPassword  = randomEncodedPassword();

    UserResponseBuilder() { }

    public UserResponseBuilder email(String email) {
        this.email = email;
        return this;
    }

    public UserResponse build() {
        return new UserResponse(id, givenName, familyName, email, encodedPassword);
    }

}
