package com.vabland.api.user.presentation;

import com.vabland.api.user.domain.service.UserCreationService;
import com.vabland.api.user.domain.service.UserSearchService;
import com.vabland.api.user.presentation.request.UserCreationRequest;
import com.vabland.api.user.presentation.response.UserResponse;
import com.vabland.api.user.utils.fixture.Fixture;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

import static com.vabland.api.user.utils.random.RandomAttributeUtils.randomEmail;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpStatus.*;

@ExtendWith(MockitoExtension.class)
@DisplayName("UserController Class")
class UserControllerTest {

    @Mock
    private UserCreationService userCreationService;

    @Mock
    private UserSearchService userSearchService;

    @InjectMocks
    private UserController userController;

    @Nested
    @DisplayName("create Method")
    class CreateMethod {

        @Test
        @DisplayName("returns ResponseEntity with status OK and with the same object returned from UserCreationService")
        void returnsUserCreationResponseWithUserIdWhenServiceCreatesUserWithSuccess() {
            UserResponse userResponse = Fixture.userResponse().build();
            UserCreationRequest request = mock(UserCreationRequest.class);
            when(userCreationService.create(request)).thenReturn(userResponse);

            ResponseEntity<UserResponse> response = userController.create(request);

            assertThat(response.getStatusCode()).isEqualTo(CREATED);
            assertThat(response.getBody()).isEqualTo(userResponse);
        }

    }

    @Nested
    @DisplayName("findByEmail Method")
    class FindByEmailMethod {

        @Test
        @DisplayName("should returns ResponseEntity with status OK and with the same object returned from UserCreationService when it responds with an Optional filled with UserResponse")
        void returnsUserAndOKWhenEmailMatchesOneRegistered() {
            String email = randomEmail();
            UserResponse userResponse = Fixture.userResponse()
                    .email(email)
                    .build();

            when(userSearchService.findByEmail(email)).thenReturn(Optional.of(userResponse));

            ResponseEntity<UserResponse> response = userController.findByEmail(email);

            assertThat(response.getStatusCode()).isEqualTo(OK);
            assertThat(response.getBody()).isEqualTo(userResponse);
        }

        @Test
        @DisplayName("should returns ResponseEntity with status NOT FOUND and with the empty body when UserCreationService returns and empty Optional")
        void returnsNotFoundWhenEmailIsNotAssociatedWithAnyUser() {
            String email = randomEmail();
            when(userSearchService.findByEmail(email)).thenReturn(Optional.empty());

            ResponseEntity<UserResponse> response = userController.findByEmail(email);

            assertThat(response.getStatusCode()).isEqualTo(NOT_FOUND);
        }

    }
}
