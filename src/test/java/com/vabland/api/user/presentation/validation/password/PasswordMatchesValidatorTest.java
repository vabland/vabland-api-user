package com.vabland.api.user.presentation.validation.password;

import com.vabland.api.user.presentation.request.UserCreationRequest;
import com.vabland.api.user.utils.fixture.Fixture;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.validation.ConstraintValidatorContext;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;
import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
@DisplayName("PasswordMatchesValidator Class")
class PasswordMatchesValidatorTest {

    @Mock
    private ConstraintValidatorContext constraintValidatorContext;

    @InjectMocks
    private PasswordMatchesValidator passwordMatchesValidator;

    @Nested
    @DisplayName("isValid Method")
    class IsValidMethod {

        @Test
        @DisplayName("should returns true when password and matching password matches")
        void passwordMatches() {
            UserCreationRequest userCreationRequest = Fixture.userCreationRequest().build();

            boolean valid = passwordMatchesValidator.isValid(userCreationRequest, constraintValidatorContext);

            assertThat(valid).isTrue();
        }

        @Test
        @DisplayName("should returns false when password and matching password doesn't match")
        void passwordDoesNotMatches() {
            UserCreationRequest userCreationRequest = Fixture.userCreationRequest()
                    .matchingPassword(randomAlphanumeric(10))
                    .build();

            boolean valid = passwordMatchesValidator.isValid(userCreationRequest, constraintValidatorContext);

            assertThat(valid).isFalse();
        }

        @Test
        @DisplayName("should returns false when password is null")
        void isInvalidWhenPasswordIsNull() {
            UserCreationRequest userCreationRequest = Fixture.userCreationRequest()
                    .password(null)
                    .build();

            boolean valid = passwordMatchesValidator.isValid(userCreationRequest, constraintValidatorContext);

            assertThat(valid).isFalse();
        }

        @Test
        @DisplayName("should returns false when matchingPassword is null")
        void isInvalidWhenMatchingPasswordIsNull() {
            UserCreationRequest userCreationRequest = Fixture.userCreationRequest()
                    .matchingPassword(null)
                    .build();

            boolean valid = passwordMatchesValidator.isValid(userCreationRequest, constraintValidatorContext);

            assertThat(valid).isFalse();
        }

    }
}