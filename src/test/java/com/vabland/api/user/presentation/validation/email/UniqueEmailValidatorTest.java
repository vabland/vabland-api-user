package com.vabland.api.user.presentation.validation.email;

import com.vabland.api.user.infrastructure.nosql.UserEntityRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.validation.ConstraintValidatorContext;

import static com.vabland.api.user.utils.random.RandomAttributeUtils.randomEmail;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@DisplayName("UniqueEmailValidator Class")
class UniqueEmailValidatorTest {

    @Mock
    private UserEntityRepository userEntityRepository;

    @Mock
    private ConstraintValidatorContext constraintValidatorContext;

    @InjectMocks
    private UniqueEmailValidator uniqueEmailValidator;

    @Nested
    @DisplayName("isValid Method")
    class IsValidMethod {

        @Test
        @DisplayName("Returns true when email is  not associated to any other user")
        void isValidEmailWhenItMatchesTheRegex() {
            String email = randomEmail();
            when(userEntityRepository.existsByEmail(email)).thenReturn(false);

            boolean valid = uniqueEmailValidator.isValid(email, constraintValidatorContext);

            assertThat(valid).isTrue();
        }

        @Test
        @DisplayName("Returns false when email is already associated to another user")
        void isInvalidEmailWhenItWasAlreadyUsed() {
            String email = randomEmail();
            when(userEntityRepository.existsByEmail(email)).thenReturn(true);

            boolean valid = uniqueEmailValidator.isValid(email, constraintValidatorContext);

            assertThat(valid).isFalse();
        }

        @Test
        @DisplayName("Returns false when email is null")
        void isInvalidWhenEmailIsNull() {
            boolean valid = uniqueEmailValidator.isValid(null, constraintValidatorContext);

            assertThat(valid).isFalse();
        }

    }
}