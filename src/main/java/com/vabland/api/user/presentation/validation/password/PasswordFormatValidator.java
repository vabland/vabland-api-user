package com.vabland.api.user.presentation.validation.password;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class PasswordFormatValidator implements ConstraintValidator<ValidPasswordFormat, String> {

    private Pattern pattern;
    private Matcher matcher;
    private static final String AT_LEAST_ONE_DIGIT = "(?=.*[0-9])";
    private static final String AT_LEAST_ONE_LOWER_CASE = "(?=.*[a-z])";
    private static final String AT_LEAST_ONE_UPPER_CASE = "(?=.*[A-Z])";
    private static final String MIN_8_CHARS = ".{8,}";
    private static final String PASSWORD_PATTERN = "^" +
            AT_LEAST_ONE_DIGIT +
            AT_LEAST_ONE_LOWER_CASE +
            AT_LEAST_ONE_UPPER_CASE +
            MIN_8_CHARS +
            "$";

    @Override
    public void initialize(ValidPasswordFormat constraintAnnotation) { }

    @Override
    public boolean isValid(String password, ConstraintValidatorContext context){
        return (validatePassword(password));
    }

    private boolean validatePassword(String password) {
        if (password == null) {
            return false;
        }

        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);
        return matcher.matches();
    }
}
