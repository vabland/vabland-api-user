package com.vabland.api.user.presentation.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class UserResponse {

    private String id;
    private String givenName;
    private String familyName;
    private String email;
    private String encodedPassword;

}
