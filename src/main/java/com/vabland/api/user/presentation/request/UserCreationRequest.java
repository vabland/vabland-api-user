package com.vabland.api.user.presentation.request;

import com.vabland.api.user.presentation.validation.email.UniqueEmail;
import com.vabland.api.user.presentation.validation.email.ValidEmailFormat;
import com.vabland.api.user.presentation.validation.password.PasswordMatches;
import com.vabland.api.user.presentation.validation.password.ValidPasswordFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@PasswordMatches
public class UserCreationRequest {

    @NotNull
    @NotEmpty
    @Size(min = 2, max=180)
    private String givenName;

    @NotNull
    @NotEmpty
    @Size(min = 2, max=180)
    private String familyName;

    @NotNull
    @NotEmpty
    @ValidEmailFormat
    @UniqueEmail
    @Size(min = 3, max=180)
    private String email;

    @NotNull
    @NotEmpty
    @Size(min = 8, max=40)
    @ValidPasswordFormat
    private String password;
    private String matchingPassword;

}
