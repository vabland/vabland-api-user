package com.vabland.api.user.presentation.validation.password;


import com.vabland.api.user.presentation.request.UserCreationRequest;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

class PasswordMatchesValidator implements ConstraintValidator<PasswordMatches, UserCreationRequest> {

    @Override
    public void initialize(PasswordMatches constraintAnnotation) {}

    @Override
    public boolean isValid(UserCreationRequest request, ConstraintValidatorContext context){
        if (request.getPassword() == null) {
            return false;
        }

        return request.getPassword().equals(request.getMatchingPassword());
    }

}
