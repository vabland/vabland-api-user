package com.vabland.api.user.presentation;

import com.vabland.api.user.domain.service.UserCreationService;
import com.vabland.api.user.domain.service.UserSearchService;
import com.vabland.api.user.presentation.request.UserCreationRequest;
import com.vabland.api.user.presentation.response.UserResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

import static org.springframework.http.HttpStatus.CREATED;

@RestController
class UserController {

    @Autowired
    private UserCreationService userCreationService;

    @Autowired
    private UserSearchService userSearchService;

    @PostMapping("/users")
    ResponseEntity<UserResponse> create(@Valid @RequestBody UserCreationRequest request) {
        UserResponse response = userCreationService.create(request);
        return ResponseEntity.status(CREATED).body(response);
    }

    @GetMapping("/users/{email}")
    ResponseEntity<UserResponse> findByEmail(@PathVariable("email") String email) {
        Optional<UserResponse> response = userSearchService.findByEmail(email);

        if (response.isPresent()) {
            return ResponseEntity.ok(response.get());
        }
        return ResponseEntity.notFound().build();
    }
}
