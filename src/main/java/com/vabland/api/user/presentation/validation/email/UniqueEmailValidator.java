package com.vabland.api.user.presentation.validation.email;

import com.vabland.api.user.infrastructure.nosql.UserEntity;
import com.vabland.api.user.infrastructure.nosql.UserEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
class UniqueEmailValidator implements ConstraintValidator<UniqueEmail, String> {

    @Autowired
    private UserEntityRepository userEntityRepository;

    @Override
    public void initialize(UniqueEmail constraintAnnotation) { }

    @Override
    public boolean isValid(String email, ConstraintValidatorContext context){
        return (validateEmail(email));
    }

    private boolean validateEmail(String email) {
        if (email == null) {
            return false;
        }

        return !userEntityRepository.existsByEmail(email);
    }
}
