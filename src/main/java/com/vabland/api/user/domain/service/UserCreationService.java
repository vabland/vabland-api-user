package com.vabland.api.user.domain.service;

import com.vabland.api.user.infrastructure.nosql.UserEntity;
import com.vabland.api.user.infrastructure.nosql.UserEntityRepository;
import com.vabland.api.user.presentation.request.UserCreationRequest;
import com.vabland.api.user.presentation.response.UserResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class UserCreationService {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserEntityRepository userEntityRepository;

    public UserResponse create(UserCreationRequest request) {
        UserEntity userEntity = userCreationRequestToUser(request);

        userEntity = userEntityRepository.save(userEntity);

        return userToUserResponse(userEntity);
    }

    private UserEntity userCreationRequestToUser(UserCreationRequest request) {
        String encodedPassword = passwordEncoder.encode(request.getPassword());
        return new UserEntity(request.getGivenName(),
                request.getFamilyName(),
                request.getEmail(),
                encodedPassword);
    }

    private UserResponse userToUserResponse(UserEntity savedUserEntity) {
        return new UserResponse(
                savedUserEntity.getId(),
                savedUserEntity.getGivenName(),
                savedUserEntity.getFamilyName(),
                savedUserEntity.getEmail(),
                savedUserEntity.getEncodedPassword());
    }

}
