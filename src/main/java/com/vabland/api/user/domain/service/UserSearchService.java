package com.vabland.api.user.domain.service;

import com.vabland.api.user.infrastructure.nosql.UserEntity;
import com.vabland.api.user.infrastructure.nosql.UserEntityRepository;
import com.vabland.api.user.presentation.response.UserResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class UserSearchService {

    @Autowired
    private UserEntityRepository userEntityRepository;

    public Optional<UserResponse> findByEmail(String email) {
        List<UserEntity> userOptional = userEntityRepository.findByEmail(email);

        if (userOptional.isEmpty()) return Optional.empty();

        return Optional.of(new UserResponse(
                userOptional.get(0).getId(),
                userOptional.get(0).getGivenName(),
                userOptional.get(0).getFamilyName(),
                userOptional.get(0).getEmail(),
                userOptional.get(0).getEncodedPassword()));
    }
}
