package com.vabland.api.user.application.configuration;

import com.google.api.gax.core.CredentialsProvider;
import com.google.cloud.NoCredentials;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnProperty(name = "spring.cloud.gcp.emulator-enabled", havingValue = "true")
public class GcpConfiguration {

    @Bean
    public CredentialsProvider credentialsProvider() {
        return NoCredentials::getInstance;
    }

}
