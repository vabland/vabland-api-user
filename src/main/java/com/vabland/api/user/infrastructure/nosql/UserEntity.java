package com.vabland.api.user.infrastructure.nosql;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.cloud.gcp.data.datastore.core.mapping.Entity;
import org.springframework.data.annotation.Id;

import java.time.LocalDateTime;
import java.util.UUID;

@Entity(name = "users")
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class UserEntity {

    @Id
    private String id;

    private String givenName;
    private String familyName;
    private String email;
    private String encodedPassword;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;

    public UserEntity(String givenName, String familyName, String email, String encodedPassword) {
        this.id = randomId();
        this.givenName = givenName;
        this.familyName = familyName;
        this.email = email;
        this.encodedPassword = encodedPassword;
        createdAt = LocalDateTime.now();
        updatedAt = createdAt;
    }

    public void preUpdate() {
        updatedAt = LocalDateTime.now();
    }

    private String randomId() {
        return UUID.randomUUID().toString();
    }

    @Override
    public String toString() {
        return "User{" +
                "givenName=" + this.givenName +
                ", familyName='" + this.familyName + '\'' +
                ", email='" + this.email + '\'' +
                ", encodedPassword=" + this.encodedPassword + '\'' +
                ", createdAt=" + this.createdAt.toString() + '\'' +
                ", updatedAt=" + this.updatedAt.toString() +
                '}';
    }

}
