package com.vabland.api.user.infrastructure.nosql;

import org.springframework.cloud.gcp.data.datastore.repository.DatastoreRepository;

import java.util.List;

public interface UserEntityRepository extends DatastoreRepository<UserEntity, String> {
    List<UserEntity> findByEmail(String email);

    boolean existsByEmail(String email);
}
